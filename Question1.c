#include<stdio.h>

int main()
{
	/*C program to calculate the sum and average of any three given integers and print the output*/
	int n1,n2,n3,sum=0,average;

	/*Ask user to enter any three integer numbers*/
	printf ("Enter any 3 integers:");
	scanf ("%d %d %d",&n1,&n2,&n3);

	sum = n1 + n2 + n3;
        
	/*Print the sum*/
	printf ("The sum of the numbers you entered is %d.\n",sum);

	average = sum/3;

	/*Print the average to the nearest whole number*/
	printf ("The average of the numbers you entered is %d.\n",average);

	return 0;
}

