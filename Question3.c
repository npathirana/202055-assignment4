#include<stdio.h>

int main()
{
	/*C program to swap two numbers without using a temporary third variable*/

	int x,y;

	/*Ask user to enter two numbers*/
	printf ("Enter the first number:");
	scanf ("%d",&x);
	
	printf ("Enter the second number:");
        scanf ("%d",&y);

	/*Code to swap the two numbers using arithmetic operators*/
	x = x + y;
	y = x - y;
	x = x - y;

	printf ("After swapping: The first number is %d and the second number is %d.\n",x,y);

	return 0;
}
