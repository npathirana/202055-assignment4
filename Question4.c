#include<stdio.h>

int main()
{
	/*C program to convert temperature given in celsius to Fahrenheit*/

	float tC,tF;

	printf ("Enter the temperature in Celsius:");
	scanf ("%f",&tC);
	
	/*Convert the input temperature to Fahrenheit*/
	tF = (tC * 9/5) + 32;
	
	/*Print the temperature in Fahrenheit upto two decimal places*/
	printf ("The temperature in Fahrenheit is %.2f.\n",tF);
        
	return 0;
}

