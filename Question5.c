#include<stdio.h>

int main ()
{
	/*C program to demonstrate bitwise operators*/
	
	int A=10, B=15;
	
	printf ("A is equal to 10 and B is equal to 15\n");

	/*Bitwise AND operator*/
	printf ("The value of A & B is %d.\n",A&B);

	/*Bitwise OR operator*/
	printf ("The value of A | B is %d.\n",A|B);

	/*Bitwise Complement operator*/
	printf ("The bitwise complement of A is %d.\n",~A);

	/*Left shift operator*/
	printf ("Left shift A by 3; The value is %d.\n",A<<3);

	/*Right shift operator*/
	printf ("Right shift B by 3; The value is %d.\n",B>>3);

	
	return 0;
}

