#include<stdio.h>

int main()
{
	
	/*C program to calculate the volume of a cone where the user inputs the height and the radius of the cone*/

	float h,r,pi,volume;
	
	printf ("Enter the height of the cone:");
	scanf ("%f",&h);

	printf ("Enter the radius of the cone:");
	scanf ("%f",&r);
	
	/*Assign the approximate value of pi*/
	pi= 3.14;
	
	/*Calculate the volume*/
	volume = pi * r * r * h/3;
	
	/*Print the volume of the cone to three decimal places*/
	printf ("The volume of the cone is %.3f .\n",volume);

	return 0;
}



